import java.util.Scanner;
public class Application{

	public static void main(String[] args){
		
		//Student 1
		Student firstStudent=new Student();
		firstStudent.setFirstName("Tommy");
		firstStudent.setLastName("Tran");
		firstStudent.setGrade(90);
		firstStudent.setGender("male");
		
		
		//Student 2
		Student secondStudent=new Student();
		secondStudent.setFirstName("Noah");
		//secondStudent.lastName="IForgot";
		secondStudent.setLastName("IForgot");
		secondStudent.setGrade(80);
		secondStudent.setGender("Female");
		
		//print
		System.out.println(firstStudent.getFirstName());
		System.out.println(firstStudent.getLastName());
		System.out.println(firstStudent.getGrade());
		System.out.println(firstStudent.getGender());
		
		System.out.println(secondStudent.getFirstName());
		System.out.println(secondStudent.getLastName());
		System.out.println(secondStudent.getGrade());
		System.out.println(secondStudent.getGender());
		
		//methods
		firstStudent.fullName();
		secondStudent.fullName();
		System.out.println("Inside method gradePlusBonus: "+firstStudent.gradePlusBonus(7));
		System.out.println("Inside method gradePlusBonus: "+secondStudent.gradePlusBonus(5));
		
		//Array
		Student[] section3=new Student[3];
		section3[0]=firstStudent;
		section3[1]=secondStudent;
		section3[2]=new Student();
		
		section3[2].setFirstName("Emmanuelle");
		//section3[2].lastName="What's her last name?";
		section3[2].setLastName("What's her last name?");
		section3[2].setGrade(95);
		section3[2].setGender("female");
		
		//print from array
		System.out.println(section3[0].getFirstName());
		
		System.out.println(section3[2].getFirstName());
		System.out.println(section3[2].getLastName());
		System.out.println(section3[2].getGrade());
		System.out.println(section3[2].getGender());
		
		
		//print amountLearnt
		
		for(int i=0;i<section3.length;i++){
			System.out.println(section3[i].amountLearnt);
		}
		
		System.out.println();
		
		//learn method
		
		Scanner sc=new Scanner(System.in);
		System.out.println("Input amountStudied");
		int amountStudied=sc.nextInt();
		
		System.out.println();
		
		section3[section3.length-1].learn(amountStudied);
		section3[section3.length-1].learn(amountStudied);
		
		for(int i=0;i<section3.length;i++){
			System.out.println(section3[i].amountLearnt);
		}
		
		System.out.println("Using learn() method with positive and negative input");
		System.out.println(section3[0].amountLearnt);
		section3[0].learn(5);
		
		System.out.println(section3[0].amountLearnt);
		section3[1].learn(-5);
		
		System.out.println(section3[0].amountLearnt);
		
		
		//part 3
		System.out.println("-----PART 3----");
		
		System.out.println(section3[0].getFirstName());
		System.out.println(section3[0].getLastName());
		System.out.println(section3[0].getGrade());
		System.out.println(section3[0].getAmountLearnt());
		
		
		// part3.9
		System.out.println("Printing Student 1 lastname before and after set method");
		
		System.out.println(section3[0].getLastName());
		System.out.println("Changing with set");
		section3[0].setLastName("NewLastName");
		System.out.println(section3[0].getLastName());
		
		System.out.println();
		System.out.println("Updated lab");
		//Updated lab4
		
		Student newSt=new Student("Tommy","Tran",80);
		newSt.setGender("Male");
		
		System.out.println(newSt.getFirstName());
		System.out.println(newSt.getLastName());
		System.out.println(newSt.getGrade());
		System.out.println(newSt.getGender());
		System.out.println(newSt.getAmountLearnt());
		
	}
	
	
}