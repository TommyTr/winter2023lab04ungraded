public class Student{
	private String firstName;
	private String lastName;
	private int grade;
	private String gender;
	public int amountLearnt;
	
	public Student(String firstName,String lastName,int grade){
		this.firstName=firstName;
		this.lastName=lastName;
		this.grade=grade;
		this.gender="Not defined";
		this.amountLearnt=0;
	}
	
	public void fullName(){
		System.out.println(firstName+" "+lastName);
	}
	
	public int gradePlusBonus(int bonus){
		return grade+bonus;
	}
	
	public void learn(int amountStudied){
		if(amountStudied >0){
			this.amountLearnt=this.amountLearnt+amountStudied;
		}
	}
	
	public String getFirstName(){
		return this.firstName;
	}
	public String getLastName(){
		return this.lastName;
	}
	public int getGrade(){
		return this.grade;
	}
	public String getGender(){
		return this.gender;
	}
	public int getAmountLearnt (){
		return this.amountLearnt;
	}
	
	public void setLastName(String lastName){
		this.lastName=lastName;
	}
	public void setFirstName(String firstName){
		this.firstName=firstName;
	}
	public void setGrade(int grade){
		this.grade=grade;
	}
	public void setGender(String gender){
		this.gender=gender;
	}
	
	public void setAmountLearnt(int amountLearnt){
		this.amountLearnt=amountLearnt;

	
}